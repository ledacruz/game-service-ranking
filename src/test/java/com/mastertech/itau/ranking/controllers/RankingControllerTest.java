package com.mastertech.itau.ranking.controllers;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mastertech.itau.ranking.controllers.RankingController;
import com.mastertech.itau.ranking.models.Ranking;
import com.mastertech.itau.ranking.repository.RankingRepository;

import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.springframework.http.MediaType;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.Arrays;

@RunWith(SpringRunner.class)
@WebMvcTest(RankingController.class)
public class RankingControllerTest {

	@MockBean
	RankingRepository rankingRepository;

	@Autowired
	RankingController rankingController;

	ObjectMapper mapper = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

/*	@Test
	public void inserirRankingTest() throws Exception {

		Ranking ranking = new Ranking();

		ranking.setGameId("1");
		ranking.setPlayerName("Leda");
		ranking.setHits("9");
		ranking.setMisses("1");
		ranking.setTotal("9/10");

		when(rankingRepository.save(any(Ranking.class))).thenReturn(ranking);

		String json = mapper.writeValueAsString(ranking);
		mockMvc.perform(post("/ranking/inserir").content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$.hits", equalTo("9")));

	}

	@Test
	public void buscarTodosOsRankingsTest() throws Exception {

		Ranking ranking = new Ranking();

		ranking.setGameId("1");
		ranking.setPlayerName("Leda");
		ranking.setHits("9");
		ranking.setMisses("1");
		ranking.setTotal("9/10");

		when(rankingRepository.findAllByOrderByHitsDesc()).thenReturn(Arrays.asList(ranking));

		String json = mapper.writeValueAsString(ranking);

		mockMvc.perform(get("/ranking/").content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].playerName", equalTo("Leda")));

	}

	@Test
	public void buscarRankingsPorJogoTest() throws Exception {

		Ranking ranking = new Ranking();

		ranking.setGameId("1");
		ranking.setPlayerName("Leda");
		ranking.setHits("9");
		ranking.setMisses("1");
		ranking.setTotal("9/10");

		when(rankingRepository.findByGameIdOrderByHitsDesc(Integer.parseInt(ranking.getGameId())))
				.thenReturn(Arrays.asList(ranking));

		String json = mapper.writeValueAsString(ranking);

		mockMvc.perform(get("/ranking/1").content(json).contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$[0].nomeJogador", equalTo("Leda")));

	}
*/
}
