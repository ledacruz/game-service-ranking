package com.mastertech.itau.ranking.repository;

import org.springframework.data.repository.CrudRepository;

import com.mastertech.itau.ranking.models.Ranking;

public interface RankingRepository extends CrudRepository<Ranking, Long> {
	
	public Iterable<Ranking> findByGameIdOrderByHitsDesc(int gameId);
	
	public Iterable<Ranking> findAllByOrderByHitsDesc();

}

