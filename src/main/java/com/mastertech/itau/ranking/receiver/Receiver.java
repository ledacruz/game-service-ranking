package com.mastertech.itau.ranking.receiver;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.mastertech.itau.ranking.models.Ranking;
import com.mastertech.itau.ranking.repository.RankingRepository;

@Component
public class Receiver {

	@Autowired
	RankingRepository rankingRepository;

	@JmsListener(destination = "c2.queue.ranking")
	public void receiveMessage(HashMap<String, String> msg) {
		// System.out.println(msg);
		// InputRanking inputRanking = new Gson().fromJson(msg, InputRanking.class);

		Ranking ranking = new Ranking();

		ranking.setGameId(msg.get("gameId"));
		ranking.setHits(msg.get("hits"));
		ranking.setMisses(msg.get("misses"));
		ranking.setPlayerName(msg.get("playerName"));
		ranking.setTotal(msg.get("total"));

		rankingRepository.save(ranking);
	}

	@JmsListener(destination = "c2.queue.ranking", containerFactory = "pubsub")
	public void listenTopic(String message) {
		System.out.println("New pub <" + message + ">");
	}
}
