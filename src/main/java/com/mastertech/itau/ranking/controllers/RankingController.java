package com.mastertech.itau.ranking.controllers;

import java.util.Random;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.mastertech.itau.ranking.models.Ranking;
import com.mastertech.itau.ranking.repository.RankingRepository;

@RestController
public class RankingController {

	@Autowired
	RankingRepository rankingRepository;
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@RequestMapping(method = RequestMethod.POST, path = "/ranking/inserir")
	public Ranking inserirRanking(@RequestBody Ranking ranking) {
		Random random = new Random();

		final String topico = "ranking";
		final String key = "1117222";
		final String body = "C2 - inserindo ranking " + key;

		
		ProducerRecord<String, String> record = new ProducerRecord<>(topico, key, body);		

	    kafkaTemplate.send(record);
		return rankingRepository.save(ranking);
	}

	@RequestMapping(method = RequestMethod.GET, path = "/ranking/{idJogo}")
	public Iterable<Ranking> buscarRanking(@PathVariable int idJogo) {
		return rankingRepository.findByGameIdOrderByHitsDesc(idJogo);

	}

	@RequestMapping(method = RequestMethod.GET, path = "/ranking/")
	public Iterable<Ranking> buscarRanking() {
		return rankingRepository.findAllByOrderByHitsDesc();

	}

}
